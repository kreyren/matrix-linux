# matrix-linux

This community is governed by the principles of anarcho-monarchism. The assigned monarch's only job is to ensure that the community is functional in terms of discussion about Linux and relevant topics while providing an efficient and accurate support for those who need it and to manage anyone who tries to form a government, disturb the anarchy or the itegrity of the room's function.

### Recommendations

These are 'recommendations' meaning non-enforcable set of guidelines with rationale to why you should follow them.

1. Avoid off-topic discussion when user is asking for help or others are having on-topic discussion.<br>
  -- It is a basic decency to make the channel sustainable and usable
 2. Don't ask To ask (https://dontasktoask.com)<br>
  -- So that you don't waste everyone's time and get the answers you need
 3. Try to avoid being a help vampire unless you really don't understand the subject and need additional info<br>
  -- So that you aren't dependent on the babysitting of others as such dependence limits your freedom

#### How to ask for help

Formulate the question with 'don't ask to ask' in mind while trying to optimize it for lenght and readability and mention the following people to get faster responses:

```
@5b2z4xywy:bidzan.net @4qgakdfrfp2idnwxgfputiw:qoto.org @xg9tdwhng23fpd2:midov.pl
```

For example:

```
@user:domain.tld @user:domain.tld How do you make a new directory from CLI?
```

### Recognized threats to the integrity of the room's anarchy

Recognized threats for the group members to manage, if they fail to do so then monarch's action will be taken.

#### Posting distressing content

Content that promotes distress in the room members is recognized as a threat to the room function and anarchy as it makes people not want to use the room and feel uncomfortable. Thus such content it enforcable and offending members will be muted for exponential amount of time per violation.

Recognized distressing content:
  * Child Porn
  * Gore
  * Animal Abuse
  * Porn

### Recommended ignore list

List of users that is recomended to be put in your ignore list:

* `@penaiple:plan9.rocks` -- User has been banned for 3 days for posting naked pictures of a child with terminal covering their intimate parts and saying 'rate my rice' [https://matrix.to/#/!CAKwbAWPygUtGBInCz:tchncs.de/$WcZZDhZyU6AqSK4ybPF16J367FhXMHQSzDTowNqGB2s?via=qoto.org&via=matrix.org&via=tchncs.de]. Child porn causes distress and disgust among the group members thus recognized as threat to the room's function and integrity.
* `@glowy:glowers.club:` -- Bot used to distribute childporn, gore and animal abuse which are recognized threats to the room's functionality, ban applies until this issue is addressed.
* `@penaiple:matrix.thisisjoes.site` -- Muted for sending gore[https://matrix.to/#/!CAKwbAWPygUtGBInCz:tchncs.de/$Nf7weVHGia8HqpItBhK8nis_BcjP4gOVhMfeiy8m_ZI?via=qoto.org&via=matrix.org&via=envs.net], unmuted few hours later
* `@penaiple:matrix.thisisjoes.site` -- Muted for sending gore[https://matrix.to/#/!CAKwbAWPygUtGBInCz:tchncs.de/$BfObO_NovHfVBHjQ46KzU2NDPSlRh5Zd2Bha79b2BGY?via=qoto.org&via=matrix.org&via=envs.net], unmuted shortly after and moderator given to @seecrets:midov.pl who spoke against his mute, if the threat to anarchy is repeated then both will be muted to explore alternative maintanence of the room that strips power from monarch
  * Experiment reverted shortly after
* `@chud:cia.govt.hu` -- User muted after being warned multiple times to not post gore without content warning